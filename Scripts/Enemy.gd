extends Area2D

var screensize

export var speed = 100
export var forward_step = 30
var direction

func _ready():
	screensize = get_viewport_rect().size
	direction = "left"

func _process(delta):
	var velocity = 0
	if direction == "left":
		velocity = -3
	else:
		velocity = 3
		
	position.x += velocity * speed * delta
	if position.x < -500 or position.x > screensize.x - 500:
		position.y += forward_step
		if direction == "left":
			direction = "right"
		else:
			direction = "left"